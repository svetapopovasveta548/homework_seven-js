// 1) Метод forEach обходит каждый элеменнт массива и для каждого вызывает функцию которая была прописана

// 2) Очистить массив можно с помощью свойства .length . Например у нас есть массив let arr = [1, 4, null, 5] и чтобы его очистить просто нужно прописать arr.length = 0 или arr = []

// 3) С помощью встроенного метода Array.isArray().


function filterBy(arr, type) {

	let result = arr.filter((elem) => {
		if (typeof elem == type)
			return false;

		return true;
	});

	return result;

};

console.log(filterBy(['hello', 'day', 23, '11', null, 27], "number"));

